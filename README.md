Nama Aplikasi: Kureer <br>
Kelompok: A17
<br>
Anggota:<br>
- (2006525053) Muhammad Fikri Haryanto
- (2006484596) Zeta Prawira Syah
- (2006596245) Francois Ferdinand Dahny Ginting
- (2006485112) Tsabita Sarah Indah Anggayasti

<br>
Daftar fitur:
<br> Sprint 1
<br>1. Registrasi Pengguna (Fikri)
<br>2. Login Pengguna & Kurir (Zeta)
<br>3. Daftar Pengiriman Kurir (Ferdi)
<br>4. Update Profile Pengguna (Tsabita)
<br><br>
Sprint 2
<br>5. Update Status Resi (Tsabita)
<br>6. Konfirmasi Pembayaran (Zeta)
<br>7. Form Order (Fikri)
<br>8. History Pengiriman Barang (Ferdi)
<br><br>
Sprint 3
<br>9. Menerima Receipt via Email (Zeta & Tsabita)
<br>10. Generate Resi (Ferdi)
<br>11. Tracking Resi (Fikri)
<br><br>
