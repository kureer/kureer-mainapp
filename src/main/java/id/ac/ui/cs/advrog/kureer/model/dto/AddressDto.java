package id.ac.ui.cs.advrog.kureer.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddressDto {
    private String emailUser;
    private String alamat;
    private String desaKelurahan;
    private String kecamatan;
    private String kotaKabupaten;
    private String provinsi;
    private String kodePos;

    public AddressDto(String emailUser, String alamat, String desaKelurahan,
                      String kecamatan, String kotaKabupaten, String provinsi, String kodePos) {
        this.emailUser = emailUser;
        this.alamat = alamat;
        this.desaKelurahan = desaKelurahan;
        this.kecamatan = kecamatan;
        this.kotaKabupaten = kotaKabupaten;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
    }
}
