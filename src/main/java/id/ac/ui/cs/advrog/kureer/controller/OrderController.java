package id.ac.ui.cs.advrog.kureer.controller;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderDto;
import id.ac.ui.cs.advrog.kureer.service.AddressServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.OrderServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private AddressServiceImpl addressService;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private UserServiceImpl userService;

    @GetMapping
    public String requestOrder(Model model) {
        User user = getUserLoggedIn();
        Address address = addressService.getAddressByUserId(user.getId());
        if (address == null) {
            return "redirect:/dashboard/update";
        }
        model.addAttribute("user", user);
        model.addAttribute("address", address);
        model.addAttribute("orderDto", new OrderDto());
        return "order";
    }

    @PostMapping
    public String postOrder(@ModelAttribute("orderDto") OrderDto orderDto) {
        User user = getUserLoggedIn();

        String email = user.getEmail();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("kureer.indonesia@gmail.com");
        message.setTo(email);

        Order order = orderService.save(user, orderDto);
        Address address = addressService.getAddressByUserId(user.getId());

        while (order.getCost() == null) {
            order = orderService.getOrderById(order.getId());
        }

        String subject = "Here's your order receipt for ID#" + order.getId();
        String content = "Hi, " + user.getFirstName() + "! \n\n"
                + "Berikut adalah detail order barang kamu: \n\n"
                + "Alamat Pengirim: \n"
                + "Alamat: " + address.getAlamat() + "\n"
                + "Provinsi: " + address.getProvinsi() + "\n"
                + "Kota/Kabupaten: " + address.getKotaKabupaten() + "\n"
                + "Kecamatan: " + address.getKecamatan() + "\n"
                + "Desa/Kelurahan: " + address.getDesaKelurahan() + "\n"
                + "Kode Pos: " + address.getKodePos() + "\n\n"
                + "Alamat Penerima: \n"
                + "Alamat: " + orderDto.getReceiverAlamat() + "\n"
                + "Provinsi: " + orderDto.getReceiverProvinsi() + "\n"
                + "Kota/Kabupaten: " + orderDto.getReceiverKotaKabupaten() + "\n"
                + "Kecamatan: " + orderDto.getReceiverKecamatan() + "\n"
                + "Desa/Kelurahan: " + orderDto.getReceiverDesaKelurahan() + "\n"
                + "Kode Pos: " + orderDto.getReceiverKodePos() + "\n\n"
                + "Detail Barang: \n"
                + "Tipe: " + order.getType() + "\n"
                + "Berat: " + order.getWeight() + "\n"
                + "Panjang: " + order.getLength() + "\n"
                + "Lebar: " + order.getWidth() + "\n"
                + "Tinggi: " + order.getHeight() + "\n\n"
                + "Ongkos Pengiriman: \n" + order.getCost() + "\n\n"
                + "Barangmu akan segera diambil oleh Kureer. Mohon ditunggu :) \n\n"
                + "Kureer Indonesia";

        message.setSubject(subject);
        message.setText(content);

        mailSender.send(message);

        return "redirect:/order/success";
    }

    @GetMapping("/success")
    public String successOrder(Model model) {
        User user = getUserLoggedIn();
        model.addAttribute("user", user);
        return "order_success";
    }

    private User getUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = authentication.getName();
        return userService.getUserByEmail(userEmail);
    }

}
