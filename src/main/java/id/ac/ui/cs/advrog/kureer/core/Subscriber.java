package id.ac.ui.cs.advrog.kureer.core;

public interface Subscriber {
    void update(LogTracking logTracking);
}
