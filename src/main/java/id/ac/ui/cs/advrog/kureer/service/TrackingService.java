package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Publisher;
import id.ac.ui.cs.advrog.kureer.core.Subscriber;

public interface TrackingService {
    void subscribeResi(String publisherId, String subscriberId);

    void unsubscribeResi(String publisherId, String subscriberId);
}
