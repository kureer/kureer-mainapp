package id.ac.ui.cs.advrog.kureer.core;

public interface Publisher {
    void registerSubsriber(Subscriber subscriber);

    void removeSubscriber(Subscriber subscriber);
    
    void notifySubscriber(LogTracking logTracking);
}
