package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;

public interface AddressService {
    void save(AddressDto addressDto);

    Address getAddressByUserId(Long userId);
}
