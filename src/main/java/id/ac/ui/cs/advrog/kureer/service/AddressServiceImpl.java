package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(AddressDto addressDto) {
        String emailUser = addressDto.getEmailUser();

        User user = userRepository.findByEmail(emailUser);
        Address address = addressRepository.findByUserId(user.getId());

        Address newAddress = new Address(user, addressDto.getAlamat(),
                addressDto.getDesaKelurahan(), addressDto.getKecamatan(),
                addressDto.getKotaKabupaten(), addressDto.getProvinsi(),
                addressDto.getKodePos());

        if (address != null) {
            Long id = address.getId();
            newAddress.setId(id);
        }
        addressRepository.save(newAddress);
    }

    @Override
    public Address getAddressByUserId(Long userId) {
        return addressRepository.findByUserId(userId);
    }
}
