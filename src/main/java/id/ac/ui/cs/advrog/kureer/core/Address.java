package id.ac.ui.cs.advrog.kureer.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    private String alamat;

    @Column(name = "desa_kelurahan")
    private String desaKelurahan;

    private String kecamatan;

    @Column(name = "kota_kabupaten")
    private String kotaKabupaten;

    private String provinsi;

    @Column(name = "kode_pos")
    private String kodePos;

    public Address(User user, String alamat, String desaKelurahan,
                   String kecamatan, String kotaKabupaten, String provinsi, String kodePos) {
        this.user = user;
        this.alamat = alamat;
        this.desaKelurahan = desaKelurahan;
        this.kecamatan = kecamatan;
        this.kotaKabupaten = kotaKabupaten;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
    }
}