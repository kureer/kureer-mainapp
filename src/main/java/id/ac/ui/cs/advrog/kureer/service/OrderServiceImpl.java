package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private AddressRepository addressRepository;

    @Value("${ORDER_URL}")
    private String orderUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Order save(User user, OrderDto orderDto) {
        Long userId = user.getId();
        Address userAddress = addressRepository.findByUserId(userId);

        orderDto.setUserId(userId);
        orderDto.setSenderAlamat(userAddress.getAlamat());
        orderDto.setSenderDesaKelurahan(userAddress.getDesaKelurahan());
        orderDto.setSenderKecamatan(userAddress.getKecamatan());
        orderDto.setSenderKotaKabupaten(userAddress.getKotaKabupaten());
        orderDto.setSenderProvinsi(userAddress.getProvinsi());
        orderDto.setSenderKodePos(userAddress.getKodePos());

        String postOrderUrl = orderUrl + "order";
        HttpEntity<OrderDto> request = new HttpEntity<>(orderDto);
        ResponseEntity<Order> response = restTemplate
                .postForEntity(postOrderUrl, request, Order.class);

        return response.getBody();
    }
    
    @Override
    public List<Order> getAllOrder() {
        String getAllOrderUrl = orderUrl + "order";

        ResponseEntity<Order[]> response = restTemplate
                .getForEntity(getAllOrderUrl, Order[].class);
        Order[] orderList = response.getBody();

        return List.of(Objects.requireNonNull(orderList));
    }

    @Override
    public List<Order> getListOrderByUserId(User user) {
        Long userId = user.getId();
        String getOrderByUserIdUrl = orderUrl + "order?userId=" + userId;

        ResponseEntity<Order[]> response = restTemplate
                .getForEntity(getOrderByUserIdUrl, Order[].class);
        Order[] orderList = response.getBody();

        return List.of(Objects.requireNonNull(orderList));
    }

    @Override
    public Order getOrderById(Long orderId) {
        String getOrderByIdUrl = orderUrl + "order/" + orderId;

        ResponseEntity<Order> response = restTemplate
                .getForEntity(getOrderByIdUrl, Order.class);

        return response.getBody();
    }

}
