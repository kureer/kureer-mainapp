package id.ac.ui.cs.advrog.kureer.core;

public enum Status {
    TO_BE_DELIVERED,
    ON_DELIVERY,
    DELIVERED
}