package id.ac.ui.cs.advrog.kureer.repository;

import id.ac.ui.cs.advrog.kureer.core.Resi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResiRepository extends JpaRepository<Resi, Long> {
    Resi findByOrderId(Long orderId);
}
