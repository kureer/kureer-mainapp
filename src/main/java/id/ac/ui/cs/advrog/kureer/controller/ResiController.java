package id.ac.ui.cs.advrog.kureer.controller;

import id.ac.ui.cs.advrog.kureer.core.Resi;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.model.dto.ResiDto;
import id.ac.ui.cs.advrog.kureer.service.ResiServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/resi")
public class ResiController {
    @Autowired
    private ResiServiceImpl resiService;

    @Autowired
    private UserServiceImpl userService;

    @PostMapping(value = "/{id}")
    public String updateResi(@PathVariable String id,
                             @ModelAttribute("resiDto") ResiDto resiDto,
                             @ModelAttribute("addressDto") AddressDto addressDto) {

        resiDto.setId(Long.parseLong(id));
        resiService.save(resiDto, addressDto);
        return "redirect:/kelola_pengguna";
    }

    @GetMapping(value = "/{id}")
    public String updateResi(@PathVariable Long id, Model model) {
        User user = getUserLoggedIn();
        Resi resi = resiService.getResiById(id);
        String statusResi = resi.getStatus() == null ? "" : resi.getStatus().toString();
        model.addAttribute("user", user);
        model.addAttribute("statusResi", statusResi);
        model.addAttribute("resiDto", new ResiDto());
        model.addAttribute("address", new AddressDto());
        return "update_resi";
    }

    private User getUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = authentication.getName();
        return userService.getUserByEmail(userEmail);
    }
}
