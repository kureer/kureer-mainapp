package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.*;
import id.ac.ui.cs.advrog.kureer.repository.*;
import id.ac.ui.cs.advrog.kureer.service.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ResiRepository resiRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TrackingService trackingService;

    @Value("${ORDER_URL}")
    private String orderUrl;

    @Override
    public Order save(Long id, Long extraCost) {
        String paymentOrderUrl = orderUrl + "order/" + id 
            + "/confirm-payment?extraCost=" + extraCost;
        ResponseEntity<Order> response = restTemplate
                .getForEntity(paymentOrderUrl, Order.class);

        Order order = response.getBody();
        Address orderAddress = order.getSenderAddress();

        Address currentAddress = new Address(null, orderAddress.getAlamat(),
                orderAddress.getDesaKelurahan(), orderAddress.getKecamatan(),
                orderAddress.getKotaKabupaten(), orderAddress.getProvinsi(),
                orderAddress.getKodePos());
        addressRepository.save(currentAddress);

        Resi resi = new Resi(order.getId(), currentAddress, Status.TO_BE_DELIVERED);
        resiRepository.save(resi);

        User user = userRepository.getById(order.getUserId());
        trackingService.subscribeResi(String.valueOf(resi.getId()), user.getEmail());

        return order;
    }
}
