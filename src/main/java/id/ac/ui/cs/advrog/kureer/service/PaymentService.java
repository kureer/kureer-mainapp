package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Order;

public interface PaymentService {
    Order save(Long id, Long extraCost);
}
