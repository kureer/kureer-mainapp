package id.ac.ui.cs.advrog.kureer.repository;

import id.ac.ui.cs.advrog.kureer.core.LogTracking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogTrackingRepository extends JpaRepository<LogTracking, Long> {

}
