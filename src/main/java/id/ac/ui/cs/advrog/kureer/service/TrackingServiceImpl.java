package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.LogTracking;
import id.ac.ui.cs.advrog.kureer.core.Resi;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.LogTrackingRepository;
import id.ac.ui.cs.advrog.kureer.repository.ResiRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TrackingServiceImpl implements TrackingService {

    @Autowired
    private ResiRepository resiRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LogTrackingRepository logTrackingRepository;

    @Override
    public void subscribeResi(String publisherId, String subscriberId) {
        Resi resi = resiRepository.getById(Long.parseLong(publisherId));
        User user = userRepository.findByEmail(subscriberId);

        if (!resi.getSubscribers().contains(user)) {
            resi.registerSubsriber(user);

            Date date = new Date();
            String log = "Subscribe resi " + publisherId;
            LogTracking logTracking = new LogTracking(date, log);
            logTrackingRepository.save(logTracking);

            user.update(logTracking);
            userRepository.save(user);
        }
    }

    @Override
    public void unsubscribeResi(String publisherId, String subscriberId) {
        Resi resi = resiRepository.getById(Long.parseLong(publisherId));
        User user = userRepository.findByEmail(subscriberId);

        if (resi.getSubscribers().contains(user)) {
            resi.removeSubscriber(user);

            Date date = new Date();
            String log = "Unsubscribe resi " + publisherId;
            LogTracking logTracking = new LogTracking(date, log);
            logTrackingRepository.save(logTracking);

            user.update(logTracking);
            userRepository.save(user);
        }
    }
}
