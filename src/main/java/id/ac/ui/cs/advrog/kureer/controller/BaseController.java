package id.ac.ui.cs.advrog.kureer.controller;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.Resi;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.model.dto.UserRegistrationDto;
import id.ac.ui.cs.advrog.kureer.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping
public class BaseController {

    @Autowired
    private AddressServiceImpl addressService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private ResiServiceImpl resiService;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("resiObject", null);
        model.addAttribute("resiId", null);
        return "homepage";
    }

    @PostMapping
    public String cekResi(Model model, @ModelAttribute("resi") String id) {
        if (Objects.equals(id, "")) {
            return "redirect:/";
        }
        User user = getUserLoggedIn();
        Resi resi = resiService.getResiById(Long.parseLong(id));
        boolean isSubscribe = false;
        if (resi != null) {
            isSubscribe = resi.getSubscribers().contains(user);
        }

        model.addAttribute("resiObject", resi);
        model.addAttribute("resiId", id);
        model.addAttribute("isSubscribe", isSubscribe);
        return "homepage";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/dashboard")
    public String dashboard(Model model) {
        User user = getUserLoggedIn();
        Address address = addressService.getAddressByUserId(user.getId());
        model.addAttribute("user", user);
        model.addAttribute("address", address);
        return "dashboard";
    }

    @PostMapping("/dashboard/update")
    public String updateProfile(@ModelAttribute("addressDto") AddressDto addressDto,
                                @ModelAttribute("userDto") UserRegistrationDto userDto) {
        User user = getUserLoggedIn();
        addressDto.setEmailUser(user.getEmail());
        addressService.save(addressDto);

        userDto.setEmail(user.getEmail());
        userService.updatePhoneNumber(userDto);
        return "redirect:/dashboard";
    }

    @GetMapping("/dashboard/update")
    public String updateProfile(Model model) {
        User user = getUserLoggedIn();
        Address address = addressService.getAddressByUserId(user.getId());
        model.addAttribute("user", user);
        model.addAttribute("address", address);
        model.addAttribute("addressDto", new AddressDto());
        model.addAttribute("userDto", new UserRegistrationDto());
        return "update_profile";
    }

    private User getUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = authentication.getName();
        return userService.getUserByEmail(userEmail);
    }

    @GetMapping("/kelola_pengguna")
    public String manageUser(Model model) {
        Map<User, Address> mapUserAddress = userManagementService.getMapUserAddress();
        User user = getUserLoggedIn();
        List<Order> orders = orderService.getAllOrder();
        model.addAttribute("orderList", orders);
        model.addAttribute("user", user);
        model.addAttribute("userList", mapUserAddress);
        return "kelola_Pengguna";
    }

    @GetMapping("/order_history")
    public String orderHistory(Model model) {
        User user = getUserLoggedIn();
        List<Order> orders = orderService.getListOrderByUserId(user);
        model.addAttribute("user", user);
        model.addAttribute("orderList", orders);
        return "order_history";
    }

}
