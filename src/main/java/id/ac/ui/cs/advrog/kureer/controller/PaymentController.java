package id.ac.ui.cs.advrog.kureer.controller;

import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.service.OrderServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.PaymentServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private PaymentServiceImpl paymentService;

    @GetMapping(value = "{id}")
    public String requestPayment(@PathVariable Long id, Model model) {
        User user = getUserLoggedIn();
        Order order = orderService.getOrderById(id);
        User userOrder = userService.getUserById(order.getUserId());
        String fullname = userOrder.getFirstName() + " " + userOrder.getLastName();
        String phoneNumber = userOrder.getPhoneNumber();

        model.addAttribute("user", user);
        model.addAttribute("order", order);
        model.addAttribute("fullname", fullname);
        model.addAttribute("phoneNumber", phoneNumber);
        model.addAttribute("extraCost", 0L);
        return "details_order";
    }

    @PostMapping(value = "{id}")
    public String postPayment(@PathVariable() Long id,
                              @ModelAttribute("extraCost") Long extraCost) {
        paymentService.save(id, extraCost);
        return "redirect:/payment/" + id;
    }

    private User getUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = authentication.getName();
        return userService.getUserByEmail(userEmail);
    }

}
