package id.ac.ui.cs.advrog.kureer.repository;

import id.ac.ui.cs.advrog.kureer.core.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
