package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.*;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.model.dto.ResiDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.LogTrackingRepository;
import id.ac.ui.cs.advrog.kureer.repository.ResiRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ResiServiceImpl implements ResiService {

    @Autowired
    private ResiRepository resiRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private LogTrackingRepository logTrackingRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(ResiDto resiDto, AddressDto addressDto) {

        Resi resi = resiRepository.getById(resiDto.getId());

        Address currentAddress = new Address(null, addressDto.getAlamat(),
                addressDto.getDesaKelurahan(), addressDto.getKecamatan(),
                addressDto.getKotaKabupaten(), addressDto.getProvinsi(),
                addressDto.getKodePos());
        addressRepository.save(currentAddress);

        if (resiDto.getStatus().equals("to_be_delivered")) {
            resi.setCurrentAddress(currentAddress);
            resi.setStatus(Status.TO_BE_DELIVERED);
            resiRepository.save(resi);
        } else if (resiDto.getStatus().equals("on_delivery")) {
            resi.setCurrentAddress(currentAddress);
            resi.setStatus(Status.ON_DELIVERY);
            resiRepository.save(resi);
        } else {
            resi.setCurrentAddress(currentAddress);
            resi.setStatus(Status.DELIVERED);
            resiRepository.save(resi);
        }

        Date date = new Date();
        String log = "Orderan dengan resi " + resi.getId() + " berada di " 
                + currentAddress.getAlamat() + ", "
                + currentAddress.getDesaKelurahan() + ", " 
                + currentAddress.getKecamatan() + ", "
                + currentAddress.getKotaKabupaten() + ", " 
                + currentAddress.getProvinsi() + " "
                + currentAddress.getKodePos() + " dengan status " 
                + resi.getStatus();

        LogTracking logTracking = new LogTracking(date, log);
        logTrackingRepository.save(logTracking);

        resi.notifySubscriber(logTracking);
        userRepository.saveAll(resi.getSubscribers());
    }

    @Override
    public Resi getResiByOrderId(Long orderId) {
        return resiRepository.findByOrderId(orderId);
    }

    @Override
    public Resi getResiById(Long id) {
        return resiRepository.findById(id).orElse(null);
    }

    @Override
    public List<Resi> getAllResi() {
        return resiRepository.findAll();
    }
}