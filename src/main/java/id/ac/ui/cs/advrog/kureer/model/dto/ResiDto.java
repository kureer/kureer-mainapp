package id.ac.ui.cs.advrog.kureer.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResiDto {
    private Long id;
    private Long orderId;
    private String status;

    public ResiDto(Long id, Long orderId, String status) {
        this.id = id;
        this.orderId = orderId;
        this.status = status;
    }
}