package id.ac.ui.cs.advrog.kureer.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "log_tracking")
public class LogTracking {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tanggal")
    private Date date;

    @Column(name = "detail")
    private String detail;

    public LogTracking(Date date, String detail) {
        this.date = date;
        this.detail = detail;
    }
}
