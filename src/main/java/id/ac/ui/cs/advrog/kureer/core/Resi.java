package id.ac.ui.cs.advrog.kureer.core;

import id.ac.ui.cs.advrog.kureer.core.util.ResiGeneratorUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import java.util.*;
import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "resi")
public class Resi implements Publisher {

    @Id
    @GenericGenerator(name = ResiGeneratorUtil.generatorName, 
        strategy = "id.ac.ui.cs.advrog.kureer.core.util.ResiGeneratorUtil")
    @GeneratedValue(generator = ResiGeneratorUtil.generatorName)
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name = "current_address", referencedColumnName = "id")
    private Address currentAddress;

    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToMany
    @JoinTable(
            name = "resi_subscriber",
            joinColumns = @JoinColumn(name = "resi_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> subscribers = new HashSet<>();

    public Resi(Long orderId, Address currentAddress, Status status) {
        this.orderId = orderId;
        this.currentAddress = currentAddress;
        this.status = status;
    }

    @Override
    public void registerSubsriber(Subscriber subscriber) {
        subscribers.add((User) subscriber);
    }

    @Override
    public void removeSubscriber(Subscriber subscriber) {
        subscribers.remove((User) subscriber);
    }

    @Override
    public void notifySubscriber(LogTracking logTracking) {
        for (Subscriber subscriber: subscribers) {
            subscriber.update(logTracking);
        }
    }
}
