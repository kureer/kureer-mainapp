package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserManagementServiceImpl implements UserManagementService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public Map<User, Address> getMapUserAddress() {
        List<User> userList = userRepository.findAll();
        Map<User, Address> mapUserAddress = new HashMap<>();
        for (User user : userList) {
            mapUserAddress.put(user, addressRepository.findByUserId(user.getId()));
        }

        return mapUserAddress;
    }


}
