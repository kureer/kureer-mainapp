package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.Resi;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.model.dto.ResiDto;

import java.util.List;

public interface ResiService {
    void save(ResiDto resiDto, AddressDto addressDto);

    Resi getResiByOrderId(Long orderId);

    Resi getResiById(Long id);

    List<Resi> getAllResi();
}