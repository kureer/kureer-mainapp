package id.ac.ui.cs.advrog.kureer.core;

public enum Role {
    USER,
    KURIR,
    ADMIN
}
