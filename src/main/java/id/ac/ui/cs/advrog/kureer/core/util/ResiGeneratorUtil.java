package id.ac.ui.cs.advrog.kureer.core.util;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

// taken from https://www.baeldung.com/java-generate-random-long-float-integer-double
public class ResiGeneratorUtil implements IdentifierGenerator {
    
    public static final String generatorName = "resiGenerator";

    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, 
        Object object) throws HibernateException {
        long leftLimit = 10_000_000_000_000L;
        long rightLimit = 100_000_000_000_000L;
        long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
        return generatedLong;
    }
}
