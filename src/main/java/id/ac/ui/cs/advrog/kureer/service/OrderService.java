package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderDto;

import java.util.List;

public interface OrderService {
    Order save(User user, OrderDto orderDto);

    List<Order> getAllOrder();

    List<Order> getListOrderByUserId(User user);

    Order getOrderById(Long id);
}
