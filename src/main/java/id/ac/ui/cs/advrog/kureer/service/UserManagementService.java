package id.ac.ui.cs.advrog.kureer.service;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.User;

import java.util.Map;

public interface UserManagementService {
    Map<User, Address> getMapUserAddress();
}
