package id.ac.ui.cs.advrog.kureer.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Order {
    private Long id;
    private Date createDate;
    private Long userId;
    private Address senderAddress;
    private String receiverName;
    private String receiverPhoneNumber;
    private Address receiverAddress;
    private String type;
    private Long weight;
    private Long length;
    private Long width;
    private Long height;
    private Long cost;
    private boolean payed;

    public Order(Long userId, Address senderAddress, Address receiverAddress,
                 String receiverName, String receiverPhoneNumber, String type,
                 Long weight, Long length, Long width, Long height) {
        this.userId = userId;
        this.senderAddress = senderAddress;
        this.receiverAddress = receiverAddress;
        this.receiverName = receiverName;
        this.receiverPhoneNumber = receiverPhoneNumber;
        this.type = type;
        this.weight = weight;
        this.length = length;
        this.width = width;
        this.height = height;
    }
}
