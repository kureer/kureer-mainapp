package id.ac.ui.cs.advrog.kureer.controller;

import id.ac.ui.cs.advrog.kureer.core.LogTracking;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.service.TrackingServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/tracking")
public class TrackingController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private TrackingServiceImpl trackingService;

    @GetMapping
    public String getTracking(Model model) {
        User user = getUserLoggedIn();
        List<LogTracking> trackingList = user.getLogTracking();
        Collections.reverse(trackingList);
        model.addAttribute("user", user);
        model.addAttribute("trackingList", trackingList);
        return "tracking";
    }

    @GetMapping(value = "subscribe/{resiId}")
    public String updateSubscribeResi(@PathVariable String resiId) {
        User user = getUserLoggedIn();
        trackingService.subscribeResi(resiId, user.getEmail());
        return "redirect:/tracking";
    }

    @GetMapping(value = "unsubscribe/{resiId}")
    public String updateUnsubscribeResi(@PathVariable String resiId) {
        User user = getUserLoggedIn();
        trackingService.unsubscribeResi(resiId, user.getEmail());
        return "redirect:/tracking";
    }

    private User getUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = authentication.getName();
        return userService.getUserByEmail(userEmail);
    }
}
