package id.ac.ui.cs.advrog.kureer.core;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

public class ResiTest {
    private Order order;

    private User user;

    private Address address;

    private Class<?> resiClass;

    private Resi resi;

    @BeforeEach
    public void setUp() throws Exception {
        order = new Order();
        order.setId(0L);

        user = new User();

        address = new Address(user, "dummyAlamat", "dummyDesaKelurahan", "dummyKecamatan",
                "dummyKotaKabupaten", "dummyProvinsi", "dummyKodePos");

        resiClass = Class.forName("id.ac.ui.cs.advrog.kureer.core.Resi");
        resi = new Resi(order.getId(), address, Status.TO_BE_DELIVERED);
    }

    @Test
    public void testResiIsConcreteClass() {
        assertFalse(Modifier.isAbstract(resiClass.getModifiers()));
    }


    @Test
    public void testGetIdShouldReturnCorrectly() {
        Long id = resi.getId();
        assertNull(id);
    }

    @Test
    public void testGetOrderIdShouldReturnCorrectly() {
        Long orderId = resi.getOrderId();
        assertEquals(0L, orderId);
    }

    @Test
    public void testGetAddressShouldReturnCorrectly() {
        Address addressDummy = resi.getCurrentAddress();
        assertEquals(address, addressDummy);
    }

    @Test
    public void testGetStatusShouldReturnCorrectly() {
        Status status = resi.getStatus();
        assertEquals(Status.TO_BE_DELIVERED, status);
    }

    @Test
    public void testSetIdShouldChangeId() {
        resi.setId(123L);
        Long id = resi.getId();
        assertEquals(123L, id);
    }

    @Test
    public void testSetOrderShouldChangeOrder() {
        resi.setOrderId(-1L);
        assertEquals(-1L, resi.getOrderId());
    }

    @Test
    public void testSetAddressShouldChangeOrder() {
        Address addressDummy = new Address();
        resi.setCurrentAddress(addressDummy);
        assertEquals(addressDummy, resi.getCurrentAddress());
    }

    @Test
    void testSetStatusShouldChangeStatus() {
        resi.setStatus(Status.ON_DELIVERY);
        assertEquals(Status.ON_DELIVERY, resi.getStatus());
    }
}
