package id.ac.ui.cs.advrog.kureer.controller;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advrog.kureer.core.*;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.ResiRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import id.ac.ui.cs.advrog.kureer.service.ResiServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.class)
public class ResiControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private ResiRepository resiRepository;

    @Autowired
    private ResiServiceImpl resiService;

    @Autowired
    private AddressRepository addressRepository;

    private MockMvc mockMvc;

    private Resi resi;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User kurir = userRepository.findByEmail("kurir@gmail.com");
        if (kurir == null) {
            kurir = new User();
            kurir.setEmail("kurir@gmail.com");
            kurir.setRole(Role.KURIR);
            userRepository.save(kurir);
        } else if (kurir.getRole() != Role.KURIR) {
            kurir.setRole(Role.KURIR);
            userRepository.save(kurir);
        }

        Address address = addressRepository.findByUserId(kurir.getId());
        if (address == null) {
            address = new Address();
            addressRepository.save(address);
        }

        Order order = new Order();
        order.setId(0L);

        resi = resiService.getResiById(1L);
        if (resi == null) {
            resi = new Resi();
            resi.setId(1L);
            resi.setOrderId(0L);
            resi.setCurrentAddress(address);
            resi.setStatus(Status.TO_BE_DELIVERED);
            resiRepository.save(resi);
        }
    }

    @Test
    public void testGetResiPageAnonymous() throws Exception {
        mockMvc.perform(get("/resi/1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "kurir@gmail.com", authorities = "KURIR")
    public void testGetResiPageShouldReturnResiPage() throws Exception {
        when(resiRepository.findById(anyLong())).thenReturn(Optional.of(resi));
        mockMvc.perform(get("/resi/1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("statusResi"))
                .andExpect(model().attributeExists("resiDto"))
                .andExpect(model().attributeExists("address"))
                .andExpect((handler().methodName("updateResi")))
                .andExpect(view().name("update_resi"));
    }

    @Test
    @WithMockUser(username = "kurir@gmail.com", authorities = "KURIR")
    public void testPostResiShouldUpdateResi() throws Exception {
        when(resiRepository.getById(anyLong())).thenReturn(resi);
        mockMvc.perform(post("/resi/1")
                        .with(csrf())
                        .param("status", "TO_BE_DELIVERED")
                        .param("alamat", "dummyAlamat")
                        .param("desaKelurahan", "dummyDesaKelurahan")
                        .param("kecamatan", "dummyKecamatan")
                        .param("kotaKabupaten", "dummyKotaKabupaten")
                        .param("provinsi", "dummyProvinsi")
                        .param("kodePos", "dummyKodePos"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("updateResi")))
                .andExpect(view().name("redirect:/kelola_pengguna"))
                .andExpect(redirectedUrl("/kelola_pengguna"));
    }
}
