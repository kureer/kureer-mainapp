package id.ac.ui.cs.advrog.kureer.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@AutoConfigureMockMvc
class UserRegistrationControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User user = userRepository.findByEmail("dummy@gmail.com");
        if (user != null) {
            Long userId = user.getId();
            Address address = addressRepository.findByUserId(userId);
            if (address != null) {
                addressRepository.delete(address);
            }
            userRepository.delete(user);
        }
    }

    @Test
    public void testGetRegisterShouldReturnRegisterPage() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("registerUserAccount")))
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("register"));
    }

    @Test
    public void testPostRegisterItShouldAddNewUser() throws Exception {
        mockMvc.perform(post("/registration")
                        .with(csrf())
                        .param("firstName", "firstDummyName")
                        .param("lastName", "lastDummyName")
                        .param("phoneNumber", "081234567890")
                        .param("email", "dummy@gmail.com")
                        .param("password", "dummyPassword"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("registerUserAccount")))
                .andExpect(view().name("redirect:/registration?success"))
                .andExpect(redirectedUrl("/registration?success"));
    }
}