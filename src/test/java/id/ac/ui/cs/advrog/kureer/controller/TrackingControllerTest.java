package id.ac.ui.cs.advrog.kureer.controller;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import id.ac.ui.cs.advrog.kureer.service.TrackingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.class)
public class TrackingControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private TrackingServiceImpl trackingService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User user = userRepository.findByEmail("dummy@gmail.com");
        if (user == null) {
            user = new User();
            user.setEmail("dummy@gmail.com");
            userRepository.save(user);
        }
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testGetTrackingShouldReturnCorrectly() throws Exception {
        mockMvc.perform(get("/tracking"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("trackingList"))
                .andExpect((handler().methodName("getTracking")))
                .andExpect(view().name("tracking"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testUpdateSubscribeResi() throws Exception {
        mockMvc.perform(get("/tracking/subscribe/0"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("updateSubscribeResi")))
                .andExpect(view().name("redirect:/tracking"))
                .andExpect(redirectedUrl("/tracking"));
    }
}