package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.*;
import id.ac.ui.cs.advrog.kureer.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ResiRepository resiRepository;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TrackingServiceImpl trackingService;

    @InjectMocks
    private PaymentServiceImpl paymentService;

    private Order order;

    private User user;

    @BeforeEach
    void setUp() {
        order = new Order();
        order.setId(0L);
        order.setCost((long)10);

        Address address = new Address(null, "dummyAlamat", "dummyDesaKelurahan",
                "dummyKecamatan", "dummyKotaKabupaten", "dummyProvinsi", "dummyKodePos");
        order.setSenderAddress(address);
        order.setUserId(0L);

        user = new User();
    }

    @Test
    public void testSaveOrder() {
        ResponseEntity<Order> response = new ResponseEntity<>(order, HttpStatus.OK);
        when(userRepository.getById(anyLong())).thenReturn(user);
        when(restTemplate.getForEntity(any(String.class),
                eq(Order.class)))
                .thenReturn(response);
        Order orderDummy = paymentService.save(0L, 10000L);
        assertEquals(order, orderDummy);
    }
}
