package id.ac.ui.cs.advrog.kureer.controller;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.Role;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import id.ac.ui.cs.advrog.kureer.service.OrderServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.PaymentServiceImpl;
import id.ac.ui.cs.advrog.kureer.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.class)
@Transactional
public class PaymentControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private OrderServiceImpl orderService;

    @MockBean
    private PaymentServiceImpl paymentService;

    @MockBean
    private UserServiceImpl userService;

    private MockMvc mockMvc;

    private Order order;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User kurir = userRepository.findByEmail("kurir@gmail.com");
        if (kurir == null) {
            kurir = new User();
            kurir.setEmail("kurir@gmail.com");
            kurir.setRole(Role.KURIR);
            kurir.setLastName("lastName");
            userRepository.save(kurir);
        } else if (kurir.getRole() != Role.KURIR) {
            kurir.setRole(Role.KURIR);
            kurir.setLastName("lastName");
            userRepository.save(kurir);
        }

        order = new Order();
        order.setId(1L);
        order.setSenderAddress(new Address());
        order.setUserId(1L);
    }

    @Test
    public void testGetPaymentPageAnonymous() throws Exception {
        mockMvc.perform(get("/payment/1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "kurir@gmail.com", authorities = "KURIR")
    public void testReqPaymentShouldReturnDetailsOrderPage() throws Exception {
        User userOrder = new User();
        userOrder.setFirstName("firstName");
        userOrder.setLastName("lastName");
        userOrder.setPhoneNumber("081234567890");
        
        when(orderService.getOrderById(1L)).thenReturn(order);
        when(userService.getUserById(anyLong())).thenReturn(userOrder);

        mockMvc.perform(get("/payment/1"))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("requestPayment")))
                .andExpect(view().name("details_order"))
                .andExpect(model().attributeExists("order"))
                .andExpect(model().attributeExists("extraCost"));
    }

    @Test
    @WithMockUser(username = "kurir@gmail.com", authorities = "KURIR")
    public void testPostPaymentShouldSavePayment() throws Exception {
        when(paymentService.save(anyLong(), anyLong())).thenReturn(order);
        mockMvc.perform(post("/payment/1")
                        .with(csrf())
                        .param("extraCost", "10"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("postPayment")))
                .andExpect(view().name("redirect:/payment/1"))
                .andExpect(redirectedUrl("/payment/1"));
    }
}
