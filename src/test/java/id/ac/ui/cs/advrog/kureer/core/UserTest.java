package id.ac.ui.cs.advrog.kureer.core;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.*;

class UserTest {
    private Class<?> userClass;
    private User user;

    @BeforeEach
    public void setUp() throws Exception {
        userClass = Class.forName("id.ac.ui.cs.advrog.kureer.core.User");
        user = new User("firstDummyName", "lastDummyName", "081234567890",
                "dummy@gmail.com", "dummyPassword", Role.USER);
    }

    @Test
    public void testUserIsConcreteClass() {
        assertFalse(Modifier.isAbstract(userClass.getModifiers()));
    }

    @Test
    public void testUserItemIsAUserDetails() {
        Collection<Type> interfaces = Arrays.asList(userClass.getInterfaces());

        assertTrue(interfaces.stream().anyMatch(type -> type.getTypeName()
                .equals("org.springframework.security.core.userdetails.UserDetails")));
    }

    @Test
    void testGetAuthoritiesShouldReturnCorrectly() {
        Collection<? extends GrantedAuthority> auth = user.getAuthorities();
        assertEquals(Collections.singletonList(
                new SimpleGrantedAuthority(user.getRole().name())), auth);
    }

    @Test
    public void testGetUsernameShouldReturnCorrectly() {
        String username = user.getUsername();
        assertEquals("dummy@gmail.com", username);
    }

    @Test
    public void testIsAccountNonExpired() {
        assertTrue(user.isAccountNonExpired());
    }

    @Test
    public void testIsAccountNonLocked() {
        assertTrue(user.isAccountNonLocked());
    }

    @Test
    public void testIsCredentialsNonExpired() {
        assertTrue(user.isCredentialsNonExpired());
    }

    @Test
    public void testIsEnabled() {
        assertTrue(user.isEnabled());
    }

    @Test
    public void testGetIdShouldReturnCorrectly() {
        Long id = user.getId();
        assertNull(id);
    }

    @Test
    public void testGetNameShouldReturnCorrectly() {
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        assertEquals("firstDummyName", firstName);
        assertEquals("lastDummyName", lastName);
    }

    @Test
    public void testGetPhoneNumberShouldReturnCorrectly() {
        String phoneNumber = user.getPhoneNumber();
        assertEquals("081234567890", phoneNumber);
    }

    @Test
    public void testGetEmailShouldReturnCorrectly() {
        String email = user.getEmail();
        assertEquals("dummy@gmail.com", email);
    }

    @Test
    public void testGetPasswordShouldReturnCorrectly() {
        String password = user.getPassword();
        assertEquals("dummyPassword", password);
    }

    @Test
    public void testGetRoleShouldReturnCorrectly() {
        Role role = user.getRole();
        assertEquals(Role.USER, role);
    }

    @Test
    public void testGetAddressReturnCorrectly() {
        Address address = user.getAddress();
        assertNull(address);
    }

    @Test
    public void testGetLogTrackingReturnCorrectly() {
        List<LogTracking> logTracking = user.getLogTracking();
        assertEquals(0, logTracking.size());
    }

    @Test
    public void testSetIdShouldChangeId() {
        user.setId(123L);
        Long id = user.getId();
        assertEquals(123L, id);
    }

    @Test
    public void testSetNameShouldChangeName() {
        user.setFirstName("firstName");
        user.setLastName("lastName");

        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        assertEquals("firstName", firstName);
        assertEquals("lastName", lastName);
    }

    @Test
    void testSetPhoneNumberShouldChangePhoneNumber() {
        user.setPhoneNumber("081234560987");
        String phoneNumber = user.getPhoneNumber();
        assertEquals("081234560987", phoneNumber);
    }

    @Test
    void testSetEmailShouldChangeEmail() {
        user.setEmail("dummyemail@gmail.com");
        String email = user.getEmail();
        assertEquals("dummyemail@gmail.com", email);
    }

    @Test
    void testSetPasswordShouldChangePassword() {
        user.setPassword("passwordDummy");
        String password = user.getPassword();
        assertEquals("passwordDummy", password);
    }

    @Test
    void testSetRoleShouldChangeRole() {
        user.setRole(Role.KURIR);
        Role role = user.getRole();
        assertEquals(Role.KURIR, role);
    }

    @Test
    public void testSetAddressShouldChangeAddress() {
        Address addressDummy = new Address();
        user.setAddress(addressDummy);
        Address address = user.getAddress();
        assertEquals(addressDummy, address);
    }

    @Test
    public void testSetLogTrackingReturnCorrectly() {
        user.setLogTracking(List.of(new LogTracking()));
        List<LogTracking> logTracking = user.getLogTracking();
        assertEquals(1, logTracking.size());
    }
}