package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.*;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.model.dto.ResiDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.LogTrackingRepository;
import id.ac.ui.cs.advrog.kureer.repository.ResiRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ResiServiceTest {
    @Mock
    private ResiRepository resiRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private LogTrackingRepository logTrackingRepository;

    @InjectMocks
    private ResiServiceImpl resiService;

    @Mock
    private Order order;

    private User user;

    private Resi resi;

    private ResiDto resiDto;

    private AddressDto addressDto;

    @BeforeEach
    void setUp() {
        order = new Order();
        order.setId(0L);
        user = new User();
        user.setId(0L);
        Address address = new Address();
        resi = new Resi(order.getId(), address, Status.TO_BE_DELIVERED);
        resi.registerSubsriber(user);
        resiDto = new ResiDto(1L, 2L, "to_be_delivered");
        addressDto = new AddressDto("dummy@gmail.com", "dummyAlamat",
                "dummyDesaKelurahan", "dummyKecamatan",
                "dummyKotaKabupaten", "dummyProvinsi", "dummyKodePos");
    }

    @Test
    public void testSaveResiStatusToBeDelivered() {
        resiDto.setStatus("to_be_delivered");
        lenient().when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        lenient().when(resiRepository.getById(1L)).thenReturn(resi);
        resiService.save(resiDto, addressDto);
        verify(resiRepository, times(1)).save(any(Resi.class));
        verify(logTrackingRepository, times(1)).save(any(LogTracking.class));
        verify(userRepository, times(1)).saveAll(resi.getSubscribers());
    }

    @Test
    public void testSaveResiStatusOnDelivery() {
        resiDto.setStatus("on_delivery");
        lenient().when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        lenient().when(resiRepository.getById(1L)).thenReturn(resi);
        resiService.save(resiDto, addressDto);
        verify(resiRepository, times(1)).save(any(Resi.class));
        verify(logTrackingRepository, times(1)).save(any(LogTracking.class));
        verify(userRepository, times(1)).saveAll(resi.getSubscribers());
    }

    @Test
    public void testSaveResiStatusDelivered() {
        resiDto.setStatus("delivered");
        lenient().when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        lenient().when(resiRepository.getById(1L)).thenReturn(resi);
        resiService.save(resiDto, addressDto);
        verify(resiRepository, times(1)).save(any(Resi.class));
        verify(logTrackingRepository, times(1)).save(any(LogTracking.class));
        verify(userRepository, times(1)).saveAll(resi.getSubscribers());
    }

    @Test
    public void testGetResiByOrderId() {
        Long orderId = resiDto.getOrderId();
        when(resiRepository.findByOrderId(orderId)).thenReturn(resi);
        Resi resiDummy = resiService.getResiByOrderId(orderId);
        assertEquals(resi, resiDummy);
    }

    @Test
    public void testGetResiById() {
        when(resiRepository.findById(1L)).thenReturn(Optional.ofNullable(resi));
        Resi resiDummy = resiService.getResiById(1L);
        assertEquals(resi, resiDummy);
    }

    @Test
    public void testGetAllResi() {
        List<Resi> resiList = List.of(resi);
        when(resiRepository.findAll()).thenReturn(resiList);
        assertEquals(resiList, resiService.getAllResi());
    }
}