package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.Role;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.UserRegistrationDto;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder bcryptPasswordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    private UserRegistrationDto userRegistrationDto;

    @BeforeEach
    void setUp() {
        userRegistrationDto = new UserRegistrationDto("firstDummyName", "lastDummyName",
                "081234567890", "dummy@gmail.com", "dummyPassword");
    }

    @Test
    public void testSaveUser() {
        userService.save(userRegistrationDto);
        verify(userRepository, times(1)).save(any(User.class));
        assertNull(userService.save(userRegistrationDto));
    }

    @Test
    public void testLoadUserByUsernameFound() {
        lenient().when(bcryptPasswordEncoder.encode(userRegistrationDto
                .getPassword())).thenReturn("$!&gfadAT241aghGF");
        User user = new User(userRegistrationDto.getFirstName(), userRegistrationDto.getLastName(),
                userRegistrationDto.getPhoneNumber(), userRegistrationDto.getEmail(),
                bcryptPasswordEncoder.encode(userRegistrationDto.getPassword()), Role.USER);

        lenient().when(userRepository.findByEmail(userRegistrationDto.getEmail())).thenReturn(user);
        User userDetails = (User) userService.loadUserByUsername(userRegistrationDto.getEmail());
        assertEquals("firstDummyName", userDetails.getFirstName());
        assertEquals("lastDummyName", userDetails.getLastName());
        assertEquals("081234567890", userDetails.getPhoneNumber());
        assertEquals("dummy@gmail.com", userDetails.getEmail());
        assertEquals("$!&gfadAT241aghGF", userDetails.getPassword());
        assertEquals(Role.USER, userDetails.getRole());
    }

    @Test
    public void testLoadUserByUsernameNotFound() {
        UsernameNotFoundException thrown = assertThrows(UsernameNotFoundException.class, () -> {
            userService.loadUserByUsername("dummy@gmail.com");
        });
        assertEquals(thrown.getMessage(),"Invalid username or password.");
    }

    @Test
    public void testGetUserByEmail() {
        User user = new User();
        lenient().when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        User userDummy = userService.getUserByEmail("dummy@gmail.com");

        assertEquals(user, userDummy);
        verify(userRepository, times(1)).findByEmail(any(String.class));
    }

    @Test
    public void testUpdatePhoneNumber() {
        User user = new User();
        user.setEmail(userRegistrationDto.getEmail());

        lenient().when(userRepository.findByEmail(userRegistrationDto.getEmail())).thenReturn(user);
        userService.updatePhoneNumber(userRegistrationDto);

        assertEquals(user.getPhoneNumber(), userRegistrationDto.getPhoneNumber());
        verify(userRepository, times(1)).save(any(User.class));
    }
}