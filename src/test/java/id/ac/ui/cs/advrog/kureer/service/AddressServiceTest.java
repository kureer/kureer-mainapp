package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.AddressDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AddressServiceTest {
    @Mock
    private AddressRepository addressRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AddressServiceImpl addressService;

    private User user;

    private Address address;

    private AddressDto addressDto;

    @BeforeEach
    void setUp() {
        user = new User();
        address = new Address(user, "dummyAlamat", "dummyDesaKelurahan",
                "dummyKecamatan", "dummyKotaKabupaten", "dummyProvinsi", "dummyKodePos");
        addressDto = new AddressDto("dummy@gmail.com", "dummyAlamat",
                "dummyDesaKelurahan", "dummyKecamatan",
                "dummyKotaKabupaten", "dummyProvinsi", "dummyKodePos");
    }

    @Test
    public void testSaveAddress() {
        user.setId(0L);
        lenient().when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        lenient().when(addressRepository.findByUserId(0L)).thenReturn(address);
        addressService.save(addressDto);
        verify(addressRepository, times(1)).save(any(Address.class));
    }

    @Test
    public void testGetAddressByUserId() {
        lenient().when(addressRepository.findByUserId(0L)).thenReturn(address);
        Address addressDummy = addressService.getAddressByUserId(0L);
        assertEquals(address, addressDummy);
    }
}