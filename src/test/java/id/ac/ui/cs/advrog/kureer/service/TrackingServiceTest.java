package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.LogTracking;
import id.ac.ui.cs.advrog.kureer.core.Resi;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.LogTrackingRepository;
import id.ac.ui.cs.advrog.kureer.repository.ResiRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TrackingServiceTest {

    @Mock
    private ResiRepository resiRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private LogTrackingRepository logTrackingRepository;

    @InjectMocks
    private TrackingServiceImpl trackingService;

    private Resi resi;

    private User user;

    @BeforeEach
    void setUp() {
        resi = new Resi();
        user = new User();
    }

    @Test
    public void testSubscribeResi() {
        if (resi.getSubscribers().contains(user)) {
            resi.removeSubscriber(user);
        }
        when(resiRepository.getById(0L)).thenReturn(resi);
        when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        trackingService.subscribeResi("0", "dummy@gmail.com");
        verify(resiRepository, times(1)).getById(any(Long.class));
        verify(userRepository, times(1)).findByEmail(any(String.class));
        verify(logTrackingRepository, times(1)).save(any(LogTracking.class));
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void testUnsubscribeResi() {
        if (!resi.getSubscribers().contains(user)) {
            resi.registerSubsriber(user);
        }
        when(resiRepository.getById(0L)).thenReturn(resi);
        when(userRepository.findByEmail("dummy@gmail.com")).thenReturn(user);
        trackingService.unsubscribeResi("0", "dummy@gmail.com");
        verify(resiRepository, times(1)).getById(any(Long.class));
        verify(userRepository, times(1)).findByEmail(any(String.class));
        verify(logTrackingRepository, times(1)).save(any(LogTracking.class));
        verify(userRepository, times(1)).save(any(User.class));
    }
}