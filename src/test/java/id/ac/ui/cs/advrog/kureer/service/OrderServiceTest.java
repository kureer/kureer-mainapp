package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private OrderServiceImpl orderService;

    private Address address;

    private OrderDto orderDto;

    private Order order;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setId(0L);
        address = new Address();
        orderDto = new OrderDto();
        order = new Order();
    }

    @Test
    public void testSaveOrder() {
        when(addressRepository.findByUserId(0L)).thenReturn(address);
        ResponseEntity<Order> response = new ResponseEntity<>(order, HttpStatus.OK);
        when(restTemplate.postForEntity(any(String.class),
                any(HttpEntity.class),
                eq(Order.class)))
                .thenReturn(response);
        Order orderDummy = orderService.save(user, orderDto);
        assertEquals(order, orderDummy);
        verify(addressRepository, times(1)).findByUserId(any(Long.class));
    }

    @Test
    public void testGetAllOrder() {
        Order[] orderList = {order};
        ResponseEntity<Order[]> response = new ResponseEntity<>(orderList, HttpStatus.OK);
        when(restTemplate.getForEntity(any(String.class), eq(Order[].class))).thenReturn(response);
        List<Order> allOrder = orderService.getAllOrder();
        assertEquals(allOrder, List.of(orderList));
    }

    @Test
    public void testGetListOrderByUserId() {
        Order[] orderList = {order};
        ResponseEntity<Order[]> response = new ResponseEntity<>(orderList, HttpStatus.OK);
        when(restTemplate.getForEntity(any(String.class), eq(Order[].class))).thenReturn(response);
        List<Order> allOrderByUserId = orderService.getListOrderByUserId(user);
        assertEquals(allOrderByUserId, List.of(orderList));
    }

    @Test
    public void testGetOrderById() {
        ResponseEntity<Order> response = new ResponseEntity<>(order, HttpStatus.OK);
        when(restTemplate.getForEntity(any(String.class), eq(Order.class))).thenReturn(response);
        Order orderDummy = orderService.getOrderById(0L);
        assertEquals(order, orderDummy);
    }
}