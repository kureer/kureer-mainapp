package id.ac.ui.cs.advrog.kureer.controller;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advrog.kureer.core.*;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import id.ac.ui.cs.advrog.kureer.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.class)
public class OrderControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @MockBean
    private OrderServiceImpl orderService;

    private MockMvc mockMvc;

    private User user;

    private Address address;

    private Order order;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = userRepository.findByEmail("dummy@gmail.com");
        if (user == null) {
            user = new User();
            user.setEmail("dummy@gmail.com");
            userRepository.save(user);
        }

        address = addressRepository.findByUserId(user.getId());
        if (address == null) {
            address = new Address();
            address.setUser(user);
            address.setKotaKabupaten("KAB. BREBES");
            addressRepository.save(address);
        }

        order = new Order();
        order.setCost(1000L);
    }

    @Test
    public void testGetOrderPageAnonymous() throws Exception {
        mockMvc.perform(get("/order"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testGetOrderPageShouldRedirectToDashboard() throws Exception {
        address.setUser(null);
        addressRepository.save(address);

        mockMvc.perform(get("/order"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("requestOrder")))
                .andExpect(view().name("redirect:/dashboard/update"))
                .andExpect(redirectedUrl("/dashboard/update"));

        address.setUser(user);
        addressRepository.save(address);
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testGetOrderPageShouldReturnOrderPage() throws Exception {
        mockMvc.perform(get("/order"))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("requestOrder")))
                .andExpect(view().name("order"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testPostOrderShouldSaveOrder() throws Exception {
        when(orderService.save(any(User.class), any(OrderDto.class))).thenReturn(order);
        when(orderService.getOrderById(anyLong())).thenReturn(order);
        mockMvc.perform(post("/order")
                        .with(csrf())
                        .param("receiverName", "Dummy")
                        .param("receiverPhoneNumber", "081234567890")
                        .param("alamat", "dummyReceiverAlamat")
                        .param("desaKelurahan", "dummyReceiverDesaKelurahan")
                        .param("kecamatan", "dummyReceiverKecamatan")
                        .param("kotaKabupaten", "KOTA DEPOK")
                        .param("provinsi", "dummyReceiverProvinsi")
                        .param("kodePos", "dummyReceiverKodePos")
                        .param("type", "dummyType")
                        .param("weight", "1000")
                        .param("length", "100")
                        .param("width", "50")
                        .param("height", "10"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("postOrder")))
                .andExpect(view().name("redirect:/order/success"))
                .andExpect(redirectedUrl("/order/success"));
    }

    @Test
    public void testGetOrderSuccessPageAnonymous() throws Exception {
        mockMvc.perform(get("/order/success"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testGetOrderSuccessPageShouldReturnOrderSuccessPage() throws Exception {
        mockMvc.perform(get("/order/success"))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("successOrder")))
                .andExpect(view().name("order_success"));
    }
}