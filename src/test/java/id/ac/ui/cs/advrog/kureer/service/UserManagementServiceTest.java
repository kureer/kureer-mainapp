package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
class UserManagementServiceTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserManagementServiceImpl userManagementService;

    @Test
    public void testGetMapUserAddress() {
        List<User> userList = userRepository.findAll();
        Map<User, Address> mapUserAddress = userManagementService.getMapUserAddress();
        assertEquals(userList.size(), mapUserAddress.size());
    }
}
