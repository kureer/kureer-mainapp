package id.ac.ui.cs.advrog.kureer.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advrog.kureer.core.Role;
import id.ac.ui.cs.advrog.kureer.core.User;
import id.ac.ui.cs.advrog.kureer.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@AutoConfigureMockMvc
class BaseControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User user = userRepository.findByEmail("dummy@gmail.com");
        if (user == null) {
            user = new User();
            user.setEmail("dummy@gmail.com");
            userRepository.save(user);
        }

        User kurir = userRepository.findByEmail("kurir@gmail.com");
        if (kurir == null) {
            kurir = new User();
            kurir.setEmail("kurir@gmail.com");
            kurir.setRole(Role.KURIR);
            userRepository.save(kurir);
        } else if (kurir.getRole() != Role.KURIR) {
            kurir.setRole(Role.KURIR);
            userRepository.save(kurir);
        }
    }

    @Test
    public void testGetHomePageShouldReturnHomePage() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("index")))
                .andExpect(view().name("homepage"));
    }

    @Test
    public void testGetLoginShouldReturnLoginPage() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("login")))
                .andExpect(view().name("login"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com")
    public void testGetDashboardShouldReturnDashboard() throws Exception {
        mockMvc.perform(get("/dashboard"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect((handler().methodName("dashboard")))
                .andExpect(view().name("dashboard"));
    }

    @Test
    public void testGetDashboardAnonymous() throws Exception {
        mockMvc.perform(get("/dashboard"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com")
    public void testPostDashboardShouldUpdateAddress() throws Exception {
        mockMvc.perform(post("/dashboard/update")
                        .with(csrf())
                        .param("phoneNumber", "081234567890")
                        .param("alamat", "dummyAlamat")
                        .param("desaKelurahan", "dummyDesaKelurahan")
                        .param("kecamatan", "dummyKecamatan")
                        .param("kotaKabupaten", "dummyKotaKabupaten")
                        .param("provinsi", "dummyProvinsi")
                        .param("kodePos", "dummyKodePos"))
                .andExpect(status().is(302))
                .andExpect((handler().methodName("updateProfile")))
                .andExpect(view().name("redirect:/dashboard"))
                .andExpect(redirectedUrl("/dashboard"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com")
    public void testGetUpdateProfileShouldReturnUpdatedAddress() throws Exception {
        mockMvc.perform(get("/dashboard/update"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("userDto"))
                .andExpect(model().attributeExists("addressDto"))
                .andExpect((handler().methodName("updateProfile")))
                .andExpect(view().name("update_profile"));
    }

    @Test
    public void testGetUpdateProfileAnonymous() throws Exception {
        mockMvc.perform(get("/dashboard/update"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser(username = "kurir@gmail.com", authorities = "KURIR")
    public void testGetManageUserShouldReturnManageUserPage() throws Exception {
        mockMvc.perform(get("/kelola_pengguna").with(csrf()))
                .andExpect(status().isOk())
                .andExpect((handler().methodName("manageUser")))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("userList"))
                .andExpect(view().name("kelola_Pengguna"));
    }

    @Test
    @WithMockUser(username = "dummy@gmail.com", authorities = "USER")
    public void testGetOrderHistoryShouldReturnOrderHistoryPage() throws Exception {
        mockMvc.perform(get("/order_history").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeExists("orderList"))
                .andExpect(view().name("order_history"));
    }
}