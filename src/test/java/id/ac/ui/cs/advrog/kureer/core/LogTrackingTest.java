package id.ac.ui.cs.advrog.kureer.core;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.Date;

public class LogTrackingTest {

    private Class<?> logTrackingClass;

    private LogTracking logTracking;

    private Date date;

    private String log;

    @BeforeEach
    public void setUp() throws Exception {
        logTrackingClass = Class.forName("id.ac.ui.cs.advrog.kureer.core.LogTracking");
        date = new Date();
        log = "First Track";
        logTracking = new LogTracking(date, log);
        logTracking.setId(-1L);
    }

    @Test
    public void testLogTrackingIsConcreteClass() {
        assertFalse(Modifier.isAbstract(logTrackingClass.getModifiers()));
    }

    @Test
    public void testGetIdShouldReturnCorrectly() {
        Long id = logTracking.getId();
        assertEquals(-1L, id);
    }

    @Test
    public void testGetDateShouldReturnCorrectly() {
        Date dateDummy = logTracking.getDate();
        assertEquals(date, dateDummy);
    }

    @Test
    public void testGetLogShouldReturnCorrectly() {
        String logDummy = logTracking.getDetail();
        assertEquals(log, logDummy);
    }

    @Test
    public void testSetIdShouldChangeId() {
        logTracking.setId(0L);
        Long id = logTracking.getId();
        assertEquals(0L, id);
    }

    @Test
    public void testSetDateShouldChangeDate() {
        Date dateDummy = new Date();
        logTracking.setDate(dateDummy);
        assertEquals(dateDummy, logTracking.getDate());
    }

    @Test
    public void testSetDetailShouldChangeDetail() {
        logTracking.setDetail("Second Track");
        assertEquals("Second Track", logTracking.getDetail());
    }
}